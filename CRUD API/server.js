let express = require('express');
let bodyparser = require('body-parser');
let cors=require("cors");
//create the rest object
var app = express();
//enable the ports communication
//set the JSON as MINE type
//read the json
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
app.use(cors());
app.use("/fetch",require("./fetch/fetch"));
app.use("/register",require("./register/register"));
//app.use("/update",require("./update/update"));
//app.use("/delete",require("./delete/delete"));
app.use("/login",require("./login/login"));
//assign the port no
app.listen(3000);
console.log("server listening the port no.3000");
