PROJECT: GAME REVIEW WEBSITE

TEAM MEMBERS:
Samanvitha-19WH1A0482(ECE)
Harika-19WH1A04A3(ECE)
Harshitha-19WH1A1257(IT)
Esha-19WH1A1221(IT)
Kavyanjali-19WH1A05H5(CSE)

DESCRIPTION:
Game Review is a website that involves reviews of various games.It is helpful for the gamers to know about the game.
this website includes details like:
->Which Platforms support the game
->Ratings of the game
->Trailers of the game
->The links of the game files to be downloaded.
