import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { DetailsComponent } from './components/details/details.component';
import { HomeComponent } from './components/home/home.component';
import { Game1Component } from './components/game1/game1.component';
import { Game2Component } from './components/game2/game2.component';
import { Game3Component } from './components/game3/game3.component';
import { Game4Component } from './components/game4/game4.component';
import { Game5Component } from './components/game5/game5.component';
import { Game6Component } from './components/game6/game6.component';

import { ActionComponent } from './components/action/action.component';
import { BoardGamesComponent } from './components/board-games/board-games.component';
import { EndlessRunnerComponent } from './components/endless-runner/endless-runner.component';
import { PlatformGamesComponent } from './components/platform-games/platform-games.component';
import { KidsComponent } from './components/kids/kids.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { AComponent } from './components/a/a.component';
import { BComponent } from './components/b/b.component';
import { CComponent } from './components/c/c.component';
import { DComponent } from './components/d/d.component';
import { EComponent } from './components/e/e.component';
import { FComponent } from './components/f/f.component';
import { GComponent } from './components/g/g.component';
import { HComponent } from './components/h/h.component';

import { IComponent } from './components/i/i.component';
import { JComponent } from './components/j/j.component';
import { KComponent } from './components/k/k.component';
import { LComponent } from './components/l/l.component';
import { MComponent } from './components/m/m.component';
import { NComponent } from './components/n/n.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterComponent },

  {
    path: 'home',
    component: HomeComponent,
  },
  // {
  //   path: 'search/:game-search',
  //   component: HomeComponent,
  // },
  { path: 'game1', component: Game1Component },
  { path: 'game2', component: Game2Component },
  { path: 'game3', component: Game3Component },
  { path: 'game4', component: Game4Component },
  { path: 'game5', component: Game5Component },
  { path: 'game6', component: Game6Component },
  
  { path: 'action', component: ActionComponent },
  { path: 'board-games', component: BoardGamesComponent },
  { path: 'platform-games', component: PlatformGamesComponent },
  { path: 'kids', component: KidsComponent },
  { path: 'endless-runner', component: EndlessRunnerComponent },
  { path: 'a', component: AComponent },
  { path: 'b', component: BComponent },
  { path: 'c', component: CComponent },
  { path: 'd', component: DComponent },
  { path: 'e', component: EComponent },
  { path: 'f', component: FComponent },
  { path: 'g', component: GComponent },
  { path: 'h', component: HComponent },

  { path: 'i', component: IComponent },
  { path: 'j', component: JComponent },
  { path: 'k', component: KComponent },
  { path: 'l', component: LComponent },
  { path: 'm', component: MComponent },
  { path: 'n', component: NComponent },

  



  // {path:'search/:searchItem', component:HomeComponent},

  
  

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
