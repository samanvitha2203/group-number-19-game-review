// import { Component, OnInit } from '@angular/core';
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-game6',
  templateUrl: './game6.component.html',
  styleUrls: ['./game6.component.scss']
})
export class Game6Component  {


  isReadMore = true

  showText() {
     this.isReadMore = !this.isReadMore
  }
  numberOfLikes:number=0;
  numberOfDislikes:number=0;
  likeButtonClick(){
    this.numberOfLikes++;
  }
  dislikeButtonClick(){
    this.numberOfDislikes++;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
