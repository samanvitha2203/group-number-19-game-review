import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-game2',
  templateUrl: './game2.component.html',
  styleUrls: ['./game2.component.scss']
})
export class Game2Component  {

  isReadMore = true

  showText() {
     this.isReadMore = !this.isReadMore
  }
  numberOfLikes:number=0;
  numberOfDislikes:number=0;
  likeButtonClick(){
    this.numberOfLikes++;
  }
  dislikeButtonClick(){
    this.numberOfDislikes++;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
