// import { Component, OnInit } from '@angular/core';
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-game3',
  templateUrl: './game3.component.html',
  styleUrls: ['./game3.component.scss']
})
export class Game3Component  {

  isReadMore = true

  showText() {
     this.isReadMore = !this.isReadMore
  }
  numberOfLikes:number=0;
  numberOfDislikes:number=0;
  likeButtonClick(){
    this.numberOfLikes++;
  }
  dislikeButtonClick(){
    this.numberOfDislikes++;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
