// import { Component, OnInit } from '@angular/core';
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-game5',
  templateUrl: './game5.component.html',
  styleUrls: ['./game5.component.scss']
})
export class Game5Component  {

  isReadMore = true

  showText() {
     this.isReadMore = !this.isReadMore
  }
  numberOfLikes:number=0;
  numberOfDislikes:number=0;
  likeButtonClick(){
    this.numberOfLikes++;
  }
  dislikeButtonClick(){
    this.numberOfDislikes++;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
