// import { Component, OnInit } from '@angular/core';
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-game4',
  templateUrl: './game4.component.html',
  styleUrls: ['./game4.component.scss']
})
export class Game4Component  {

  isReadMore = true

  showText() {
     this.isReadMore = !this.isReadMore
  }
  numberOfLikes:number=0;
  numberOfDislikes:number=0;
  likeButtonClick(){
    this.numberOfLikes++;
  }
  dislikeButtonClick(){
    this.numberOfDislikes++;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
